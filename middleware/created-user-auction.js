export default function ({ store, redirect }) {
  if (Object.keys(store.state.auction.createUserAuctionResult).length === 0) {
    return redirect('/auction/open')
  }
}
