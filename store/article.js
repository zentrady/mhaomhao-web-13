import { make } from 'vuex-pathify'
import { ArticleService } from '../services'

const mapData = value => value.results
const mapPage = (value) => {
  return {
    count: value.count,
    next: value.next,
    previous: value.previous
  }
}

export const state = () => ({
  articles: [],
  articlesPage: {},
  article: {}
})

export const actions = {
  async fetchArticles ({ rootState: { auth: { token } }, commit }, params) {
    try {
      const result = await ArticleService.getArticles(params)
      commit('SET_ARTICLES', mapData(result))
      commit('SET_ARTICLES_PAGE', mapPage(result))
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchArticle ({ rootState: { auth: { token } }, commit }, id) {
    try {
      const result = await ArticleService.getArticle(id)
      commit('SET_ARTICLE', result)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export const mutations = make.mutations(state)
