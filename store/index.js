import axios from 'axios'
import humps from 'humps'
import pathify from './pathify'

const cookieparser = process.server ? require('cookieparser') : undefined

export const plugins = [pathify.plugin]

export const actions = {
  async nuxtServerInit ({ commit, dispatch }, { req, res }) {
    let auth = null
    if (req.headers.cookie) {
      const parsed = cookieparser.parse(req.headers.cookie)
      try {
        auth = parsed.auth
        const resp = await axios.get('https://api.mhaomhao.com/account/me/', {
          headers: {
            Authorization: `token ${auth}`
          }
        })
        const { data } = resp
        commit('auth/SET_USER', humps.camelizeKeys(data), null, { root: true })
      } catch (err) {
        const { response } = err
        if (response.status === 401 || response.status === 403 || response.status === 404) {
          console.log('error nuxtServerInit', response)
          auth = null
          res.clearCookie('auth')
        }
      }
    }
    commit('auth/SET_TOKEN', auth, null, { root: true })
  }
}
